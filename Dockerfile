FROM ubuntu:focal as builder

ADD . /src
WORKDIR /src

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y \
    pkg-config \
    clang \
    curl \
    libssl-dev \
    && \
    curl https://sh.rustup.rs -sSf | sh -s -- -y && \
    export PATH="/root/.cargo/bin:${PATH}" && \
    cargo build --verbose --release && \
    cargo install --path .

FROM ubuntu:focal
COPY --from=builder /root/.cargo/bin/worker /usr/bin

RUN apt update && \
    apt install -y \
    libssl1.1 \
    ca-certificates 

ENV AMQP_QUEUE job_rs_stl_timecode_offset_worker
CMD worker

