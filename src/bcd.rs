pub struct Bcd {}

impl Bcd {
  pub fn convert(buffer: &[u8]) -> u32 {
    if buffer[4] == b' ' {
      println!("cas en erreur");

      return (buffer[3] as u32 - 48)
        + (buffer[2] as u32 - 48) * 10
        + (buffer[1] as u32 - 48) * 100
        + (buffer[0] as u32 - 48) * 1000;
    }

    let mut buffer = buffer.to_vec();
    buffer.reverse();
    buffer
      .iter()
      .map(|byte| match byte {
        b'0' => 0,
        b'1' => 1,
        b'2' => 2,
        b'3' => 3,
        b'4' => 4,
        b'5' => 5,
        b'6' => 6,
        b'7' => 7,
        b'8' => 8,
        b'9' => 9,
        _ => unreachable!(),
      })
      .enumerate()
      .map(|(index, value)| value as u32 * 10u32.pow(index as u32))
      .sum()
  }
}

#[test]
fn convert_bcd() {
  let data = [b'2', b'1', b'3', b'9', b'5'];
  let value = Bcd::convert(&data);
  assert_eq!(value, 21395);
}
