pub struct Tti {}

impl Tti {
  pub fn is_valid_subtitle(tti: &[u8]) -> bool {
    tti[3] != 0xFE
  }

  pub fn get_timecode_in(tti: &[u8], framerate: i32) -> i32 {
    let h: i32 = tti[5].into();
    let m: i32 = tti[6].into();
    let s: i32 = tti[7].into();
    let i: i32 = tti[8].into();
    h * (3600 * framerate) + m * (60 * framerate) + s * framerate + i
  }

  pub fn get_timecode_out(tti: &[u8], framerate: i32) -> i32 {
    let h: i32 = tti[9].into();
    let m: i32 = tti[10].into();
    let s: i32 = tti[11].into();
    let i: i32 = tti[12].into();
    h * (3600 * framerate) + m * (60 * framerate) + s * framerate + i
  }

  pub fn set_timecode_in(tti: &mut [u8], tc: i32, framerate: i32) {
    let mut tcs = tc;
    let h: i32 = tcs / (3600 * framerate);
    tcs -= h * (3600 * framerate);
    let m: i32 = tcs / (60 * framerate);
    tcs -= m * (60 * framerate);
    let s: i32 = tcs / framerate;
    tcs -= s * framerate;
    let i: i32 = tcs;

    tti[5] = h as u8;
    tti[6] = m as u8;
    tti[7] = s as u8;
    tti[8] = i as u8;
  }

  pub fn set_timecode_out(tti: &mut [u8], tc: i32, framerate: i32) {
    let mut tcs = tc;
    let h: i32 = tcs / (3600 * framerate);
    tcs -= h * (3600 * framerate);
    let m: i32 = tcs / (60 * framerate);
    tcs -= m * (60 * framerate);
    let s: i32 = tcs / framerate;
    tcs -= s * framerate;
    let i: i32 = tcs;

    tti[9] = h as u8;
    tti[10] = m as u8;
    tti[11] = s as u8;
    tti[12] = i as u8;
  }

  pub fn set_subtitle_number(tti: &mut [u8], stnumber: i32) {
    //let formatted = format!("{:05}", stnumber); // Assure 5 chiffres avec des zéros devant
    //let vec: Vec<u8> = formatted.into_bytes();

    let hi: i32 = stnumber / 256;
    let lo: i32 = stnumber - hi * 256;
    tti[2] = hi as u8;
    tti[1] = lo as u8;
  }
}
