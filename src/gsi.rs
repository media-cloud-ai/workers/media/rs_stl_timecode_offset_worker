use crate::bcd;
use std::str;

pub struct Gsi {}

impl Gsi {
  pub fn get_number_of_subtitles(gsi: &[u8]) -> u32 {
    bcd::Bcd::convert(&gsi[243..248])
  }
  pub fn get_number_of_tti(gsi: &[u8]) -> u32 {
    bcd::Bcd::convert(&gsi[238..243])
  }
  pub fn get_dfc(gsi: &[u8]) -> &str {
    return str::from_utf8(&gsi[3..11]).unwrap();
  }

  pub fn set_number_of_tti(tti: &mut [u8], number_of_tti: i32) {
    let formatted = format!("{:05}", number_of_tti); // Assure 5 chiffres avec des zéros devant
    let vec: Vec<u8> = formatted.into_bytes();

    tti[238 - 128] = vec[0];
    tti[239 - 128] = vec[1];
    tti[240 - 128] = vec[2];
    tti[241 - 128] = vec[3];
    tti[242 - 128] = vec[4];

    tti[243 - 128] = vec[0];
    tti[244 - 128] = vec[1];
    tti[245 - 128] = vec[2];
    tti[246 - 128] = vec[3];
    tti[247 - 128] = vec[4];
  }
}
