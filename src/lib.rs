mod bcd;
mod gsi;
mod tti;

use std::io::Read;
use std::io::Seek;
use std::io::SeekFrom;
use std::io::Write;
use std::io::{Error, ErrorKind};
use std::str;

pub fn process_stl(
  source_path: &str,
  timecode: &str,
  startin: &str,
  destination_path: &str,
) -> Result<(), std::io::Error> {
  std::fs::copy(source_path, destination_path)?;

  let mut file = std::fs::File::options()
    .read(true)
    .write(true)
    .open(destination_path)?;

  let mut gsi = [0; 1024];
  file.read_exact(&mut gsi)?;

  // check stl file integrity

  let number_of_subtitles = gsi::Gsi::get_number_of_subtitles(&gsi);
  log::debug!("Nombre de sous-titres : {}", number_of_subtitles);

  let number_of_tti = gsi::Gsi::get_number_of_tti(&gsi);
  log::debug!("Nombre de tti : {}", number_of_tti);

  if number_of_subtitles > number_of_tti {
    return Err(Error::new(ErrorKind::Other, "Invalid tti count!"));
  }

  let expected_size: u64 = (1024 + number_of_tti * 128).into();
  let current_size = std::fs::metadata(destination_path)?.len();

  if expected_size != current_size {
    log::debug!(
      "file size : {}, expected size {}",
      current_size,
      expected_size
    );
    return Err(Error::new(ErrorKind::Other, "Invalid file size!"));
  }

  //expected STLxx.01
  let dfc: &str = gsi::Gsi::get_dfc(&gsi);
  //let mut framerate: i32 = 25;
  let framerate = match dfc {
    "STL25.01" => 25,
    "STL24.01" => 24,
    "STL30.01" => 30,
    _ => return Err(Error::new(ErrorKind::Other, "Invalid dfc!")),
  };

  if timecode.chars().count() != 11 {
    return Err(Error::new(ErrorKind::Other, "Invalid time code len!"));
  }

  // offset string ?

  let mut h = timecode[0..2].parse::<i32>().unwrap();
  let mut m = timecode[3..5].parse::<i32>().unwrap();
  let mut s = timecode[6..8].parse::<i32>().unwrap();
  let mut i = timecode[9..11].parse::<i32>().unwrap();

  if (h < 0)
    || (m < 0)
    || (s < 0)
    || (i < 0)
    || (h > 23)
    || (m > 59)
    || (s > 59)
    || (i > (framerate - 1))
  {
    return Err(Error::new(ErrorKind::Other, "Time code out of range!"));
  }

  let mut uoffset: i32 = i + s * framerate + m * 60 * framerate + h * 3600 * framerate;

  log::debug!(
    "new time code first word: {:02}:{:02}:{:02}:{:02} framerate {} offset {}",
    h,
    m,
    s,
    i,
    framerate,
    uoffset
  );

  h = startin[0..2].parse::<i32>().unwrap();
  m = startin[3..5].parse::<i32>().unwrap();
  s = startin[6..8].parse::<i32>().unwrap();
  i = startin[9..11].parse::<i32>().unwrap();

  if (h < 0)
    || (m < 0)
    || (s < 0)
    || (i < 0)
    || (h > 23)
    || (m > 59)
    || (s > 59)
    || (i > (framerate - 1))
  {
    return Err(Error::new(ErrorKind::Other, "Time code out of range!"));
  }

  let utcin: i32 = i + s * framerate + m * 60 * framerate + h * 3600 * framerate;

  log::debug!(
    "new time code in : {:02}:{:02}:{:02}:{:02} framerate {} offset {}",
    h,
    m,
    s,
    i,
    framerate,
    utcin
  );

  if utcin > uoffset {
    return Err(Error::new(ErrorKind::Other, "TCIN > First TC!"));
  }

  // that's all folks

  let mut tti = [0; 128];
  let mut current_tti = 0;
  let mut start_tti: i32 = -1;
  let mut first_valid_tti: bool = false;

  // first pass, look for '...'s

  let start_sequence: [u8; 7] = [0x0B, 0x0B, b'.', b'.', b'.', 0x0A, 0x0A];
  while current_tti < number_of_tti && start_tti == -1 {
    file.seek(SeekFrom::Start((1024 + current_tti * 128).into()))?;
    file.read_exact(&mut tti)?;
    if tti::Tti::is_valid_subtitle(&tti) {
      let tcin: i32 = tti::Tti::get_timecode_in(&tti, framerate);
      let mut _tcout: i32 = tti::Tti::get_timecode_out(&tti, framerate);

      match find_point_point_point_start_sequence(&tti[16..128], &start_sequence) {
        Some(index) => {
          println!(
            "Subtitle {} Sequence found at index: {}",
            current_tti, index
          )
        }
        None => {
          start_tti = current_tti as i32;
          uoffset -= tcin;
          first_valid_tti = true;
          println!("Sequence not found, end of search")
        }
      }
      current_tti += 1;
    }
  }

  let mut current_tti = 0;
  while current_tti < number_of_tti {
    file.seek(SeekFrom::Start((1024 + current_tti * 128).into()))?;
    file.read_exact(&mut tti)?;

    if tti::Tti::is_valid_subtitle(&tti) {
      let mut tcin: i32 = tti::Tti::get_timecode_in(&tti, framerate);
      let mut _tcout: i32 = tti::Tti::get_timecode_out(&tti, framerate);

      if !first_valid_tti {
        first_valid_tti = true;
        uoffset -= tcin;
      }

      tcin += uoffset;
      _tcout += uoffset;

      if tcin < 0 {
        tcin = 0
      };
      if _tcout < 0 {
        _tcout = 0
      };

      if (_tcout >= 0) && (tcin >= 0) {
        tti::Tti::set_timecode_in(&mut tti, tcin, framerate);
        tti::Tti::set_timecode_out(&mut tti, _tcout, framerate);

        //log::debug!( "write new time code in {} new time code out {}", tcin, _tcout );
        file.seek(SeekFrom::Start((1024 + current_tti * 128) as u64))?;
        file.write_all(&tti)?;
      }
    }

    current_tti += 1;
  }

  // close file ??????????????????

  // on va maintenant vérifier que tout les tc des sous titres sont bien apres le tc in

  current_tti = 0;
  let mut nb_to_delete = 0;
  // start_tti = tti du premier vrai sous-titre hors "..."
  while current_tti < start_tti as u32 {
    file.seek(SeekFrom::Start((1024 + current_tti * 128).into()))?;
    file.read_exact(&mut tti)?;

    let mut tcin: i32 = tti::Tti::get_timecode_in(&tti, framerate);
    let mut _tcout: i32 = tti::Tti::get_timecode_out(&tti, framerate);
    log::debug!("tti numero : {} nouveau tcin :{}", current_tti, tcin);
    if _tcout < utcin {
      nb_to_delete += 1; // pas d'espoir pour celui la
      current_tti += 1;
    } else {
      tcin = utcin;
      if (_tcout - tcin) < 10 {
        // trop court , aucun interet
        nb_to_delete += 1;
        current_tti += 1;
      } else {
        // on valide ça et on arrete tout
        tti::Tti::set_timecode_in(&mut tti, tcin, framerate);
        file.seek(SeekFrom::Start((1024 + current_tti * 128) as u64))?;
        file.write_all(&tti)?;

        log::debug!("tti numero : {} nouveau tcin :{}", current_tti, tcin);
        current_tti = start_tti as u32; // on stoppe
      }
    }
  }

  if nb_to_delete > 0 {
    log::debug!("Number of subtitles to delete :{}", nb_to_delete,);

    current_tti = nb_to_delete;

    // on raccourcit le fichier
    while current_tti < number_of_tti {
      file.seek(SeekFrom::Start((1024 + current_tti * 128).into()))?;
      file.read_exact(&mut tti)?;
      tti::Tti::set_subtitle_number(&mut tti, (current_tti - nb_to_delete + 1) as i32); // on oublie pas de renuméroter
      file.seek(SeekFrom::Start(
        (1024 + (current_tti - nb_to_delete) * 128) as u64,
      ))?;
      file.write_all(&tti)?;
      current_tti += 1;
    }

    // on se tape le gsi*
    current_tti = 1; // bloc 1 gri
    file.seek(SeekFrom::Start((current_tti * 128).into()))?;
    file.read_exact(&mut tti)?;

    gsi::Gsi::set_number_of_tti(&mut tti, (number_of_tti - nb_to_delete) as i32);

    file.seek(SeekFrom::Start((current_tti * 128).into()))?;
    file.write_all(&tti)?;

    // on reduit la taille du fichier
    let new_size = 1024 + (number_of_tti - nb_to_delete) * 128;
    file.set_len(new_size as u64)?;
    file.flush()?;
  }

  Ok(())
}

fn find_point_point_point_start_sequence(tti: &[u8], needle: &[u8; 7]) -> Option<usize> {
  tti.windows(7).position(|window| window == needle)
}
