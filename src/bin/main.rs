use clap::Parser;
use simple_logger::SimpleLogger;
use stl_timecode_offset::process_stl;

#[derive(Parser)]
#[command(version, about, long_about = None)]
struct Cli {
  /// new time code
  #[arg(short, long, value_name = "tcpp")]
  tcpp: String,

  #[arg(short, long, value_name = "tcin")]
  tcin: String,

  /// Set the source file
  #[arg(short, long, value_name = "FILE")]
  input: String,

  /// Set the destname  file
  #[arg(short, long, value_name = "FILE")]
  output: String,
}

fn main() -> std::io::Result<()> {
  SimpleLogger::new().env().init().unwrap();

  let cli = Cli::parse();

  process_stl(&cli.input, &cli.tcpp, &cli.tcin, &cli.output)?;
  Ok(())
}
