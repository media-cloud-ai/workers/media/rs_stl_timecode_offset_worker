use mcai_worker_sdk::prelude::*;
use schemars::JsonSchema;
use serde_derive::Deserialize;
use stl_timecode_offset::process_stl;

#[derive(Debug)]
struct StlOffsetWorkerEvent {}

#[derive(Debug, Deserialize, JsonSchema)]
struct WorkerParameters {
  source_path: String,
  destination_path: String,
  tcpp: String,
  tcin: String,
}

// For opensource workers
default_rust_mcai_worker_description!();

impl McaiWorker<WorkerParameters, RustMcaiWorkerDescription> for &StlOffsetWorkerEvent {
  fn process(
    &self,
    _channel: Option<McaiChannel>,
    parameters: WorkerParameters,
    job_result: JobResult,
  ) -> Result<JobResult> {
    process_stl(
      &parameters.source_path,
      &parameters.tcpp,
      &parameters.tcin,
      &parameters.destination_path,
    )
    .unwrap();
    Ok(job_result.with_status(JobStatus::Completed))
  }
}

static WORKER_NAME_EVENT: StlOffsetWorkerEvent = StlOffsetWorkerEvent {};
fn main() {
  start_worker(&WORKER_NAME_EVENT);
}
